/****
 * main.c 
 * Authors: Sonal Chatter & Cecilie Uppard
 * Last Modified: 11/24/15
 * 
 * Summary: Opens and closes the .um file and executes the program
 ****/ 

/* INCLUDED LIBRARIES */
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <stack.h>
#include <seq.h>
#include <assert.h>
#include <except.h>
#include <stdbool.h>

#define POWER_2_32 4294967296
#define INITIAL_SIZE 10

Except_T Bitpack_Overflow = { "Overflow packing bits" };

/* Holds the current state of a given program */
typedef struct program_state {
        uint32_t *program_ptr;
        Stack_T unused_ids; 
        Seq_T memory; 
        Seq_T seg_lengths;
        uint32_t *registers;
        uint32_t program_length;
        uint32_t instruction_counter; 
}*program_state;
/*
Holds the unpacked values from a single instruction word  
typedef struct decoded_word {
        uint32_t opcode, reg_A, reg_B, reg_C, data;
}*decoded_word;
*/
/* FUNCTION DECLARATIONS */
void execute_prog(FILE *fp, int prog_length); 
uint32_t run_instruction(program_state state, uint32_t word);
void destruct(program_state state);
program_state initialize(FILE* fp, int prog_length);
//decoded_word decode(uint32_t word);
uint64_t Bitpack_getu(uint64_t word, unsigned width, unsigned lsb);
uint64_t Bitpack_newu(uint64_t word, unsigned width, unsigned lsb, 
                      uint64_t value);
bool Bitpack_fitsu(uint64_t n, unsigned width);
uint32_t create_segment(program_state state, uint32_t size); 
void put_word(program_state state, uint32_t segment_id, uint32_t offset,
              uint32_t word); 
void delete_segment(program_state state, uint32_t segment_id); 

int main(int argc, char *argv[])
{                                                               
        if (argc == 2) {
                struct stat buf;
                stat(argv[1], &buf);
                int prog_length = (buf.st_size) / 4;
                FILE *fp = fopen(argv[1], "rb");
                if (fp == NULL) {
                        exit(EXIT_FAILURE);
                } else {                                                        
                        execute_prog(fp, prog_length);
                        fclose(fp);
                }
        } else {
                exit(EXIT_FAILURE);
        }
        exit(0);                                               
}

/* Controls and executes each instruction */
void execute_prog(FILE *fp, int prog_length)
{ 
        program_state state = initialize(fp, prog_length);
        uint32_t *segment_zero = Seq_get(state->memory, 0); 
        while(1) {
                uint32_t word = segment_zero[state->instruction_counter]; 
                uint32_t opcode = run_instruction(state, word);
                /* handles special cases, load program(12) and halt(7) */
                if(opcode == 12) { 
                        segment_zero = Seq_get(state->memory, 0); 
                }
                if (opcode == 7){
                        break;
                }
                state->instruction_counter++; 
        }
        return; 
}
/* Initializes the program_state  */
program_state initialize(FILE* fp, int prog_length) 
{
        program_state state = malloc(sizeof(struct program_state)); 
        assert(state != NULL);
        uint32_t *seg_0_length = malloc(sizeof(uint32_t)); 
        assert(seg_0_length != NULL);
        state->program_length = (uint32_t)prog_length;
        uint32_t *segment0 = malloc(sizeof(uint32_t) * state->program_length);
        assert(segment0 != NULL);
        state->memory = Seq_new(INITIAL_SIZE);
        state->seg_lengths = Seq_new(INITIAL_SIZE); 
        state->unused_ids = Stack_new();

        /*mallocing length of program to store in program length sequence */
        for (uint32_t i = 0; i < state->program_length; i++) {
                uint32_t word = 0; 
                int c = fgetc(fp); 
                word = Bitpack_newu(word, 8, 24, c);
                word = Bitpack_newu(word, 8, 16, fgetc(fp));
                word = Bitpack_newu(word, 8, 8, fgetc(fp));
                word = Bitpack_newu(word, 8, 0, fgetc(fp));
                segment0[i] = word;     
        }
        
        *seg_0_length = prog_length; 
        state->program_ptr = &segment0[0]; 
        Seq_addlo(state->memory, segment0);
        Seq_addlo(state->seg_lengths, seg_0_length); 
        state->registers = (uint32_t*)malloc(sizeof(uint32_t)*8);
        /* initializing all registers to zero */        
        for(uint32_t i = 0; i < 8; i++) { 
                state->registers[i] = 0; 
        }
        state->instruction_counter = 0; 
        return state;
}
 
/* Destructs the program state and allows for a successful exit  */
void destruct(program_state state)
{
        int seq_length = Seq_length(state->memory); 
        for(int i = 0; i < seq_length; i++) {
                uint32_t *temp = Seq_get(state->memory, i);
                uint32_t *temp2 = Seq_get(state->seg_lengths, i); 
                if(temp != NULL) {
                        free(temp);
                }
                if (temp != NULL) { 
                        free(temp2);
                }
        }
        Seq_free(&state->memory);
        Seq_free(&state->seg_lengths);
        while (Stack_empty(state->unused_ids) == 0) { 
                uint32_t *id = (uint32_t *)Stack_pop(state->unused_ids);
                free(id); 
        }
        Stack_free(&state->unused_ids); 
        free(state->registers);
        free(state); 
        return; 
} 

/* Runs a given instruction, exit failure if opcode is not valid */ 
uint32_t run_instruction(program_state state, uint32_t word) 
{ 
        uint32_t opcode = (uint32_t)Bitpack_getu(word, 4, 28); 
        uint32_t reg_A;
        uint32_t reg_B;
        uint32_t reg_C;
        uint32_t data;
        if (opcode == 13) {
                reg_A = (uint32_t)Bitpack_getu(word, 3, 25);
                data = (uint32_t)Bitpack_getu(word, 25, 0); 
        }
        else {
                reg_A = (uint32_t)Bitpack_getu(word, 3, 6);
                reg_B = (uint32_t)Bitpack_getu(word, 3, 3);
                reg_C = (uint32_t)Bitpack_getu(word, 3, 0);
        }
        uint32_t *segment = NULL;
        uint32_t identifier;
        uint32_t length;
        char input = NULL;  
        switch(opcode) { 
                /* Conditional Move */
                case 0 :
                        if (state->registers[reg_C] != 0) {
                                state->registers[reg_A] = 
                                                state->registers[reg_B];
                        }
                        break; 
                /* Segmented Load */
                case 1 :
                        segment = Seq_get(state->memory, 
                                        state->registers[reg_B]);
                        state->registers[reg_A] = 
                                        segment[state->registers[reg_C]];
                        break; 
                /* Segmented Store */
                case 2 : 
                        segment = Seq_get(state->memory, 
                                        state->registers[reg_A]);
                        segment[state->registers[reg_B]] = 
                                        state->registers[reg_C];
                        break; 
                /* Addition */
                case 3 : 
                        state->registers[reg_A] = (state->registers[reg_B] +
                                        state->registers[reg_C]) % POWER_2_32;  
                        break;
                /* Multiplication */
                case 4 : 
                        state->registers[reg_A] = (state->registers[reg_B] *
                                        state->registers[reg_C]) % POWER_2_32;  
                        break; 
                /* Division */
                case 5 : 
                        state->registers[reg_A] = (state->registers[reg_B] /
                                        state->registers[reg_C]);  
                        break; 
                /* Bitwise NAND */
                case 6 : 
                        state->registers[reg_A] = ~(state->registers[reg_B] &
                                        state->registers[reg_C]);  
                        break;
                /* Halt */
                case 7 :
                        destruct(state);
                        break; 
                /* Map Segment */ 
                case 8 :
                        
                        identifier = create_segment(state, 
                                        state->registers[reg_C]);
                        state->registers[reg_B] = identifier;   
                        break; 
                /* Unmap Segment */
                case 9 : 
                        delete_segment(state, state->registers[reg_C]);
                        break; 
                /* Output */
                case 10 :
                        if ((state->registers[reg_C] <= 255)) {
                                fprintf(stdout,"%c",state->registers[reg_C]);
                        }
                        break; 
                /* Input */  
                case 11 :
                        input = fgetc(stdin);
                        if (input == EOF) {
                                state->registers[reg_C] = ~0;
                        } else {
                                state->registers[reg_C] = input;
                        }
                        break; 
                /* Load Program */
                case 12 : 
                        if(state->registers[reg_B] == 0) { 
                                state->instruction_counter = 
                                                state->registers[reg_C]-1;
                                break;
                        }
                        segment = Seq_get(state->memory, 
                                        state->registers[reg_B]);
                        length = *((uint32_t *)(Seq_get(state->seg_lengths, 
                                        state->registers[reg_B])));
                        delete_segment(state, 0);
                        identifier = create_segment(state, length);
                        for (uint32_t i = 0; i < length; i++) {
                                put_word(state, identifier, i, segment[i]);
                        }
                        state->instruction_counter = state->registers[reg_C]-1;
                        break; 
                /* Load Value */ 
                case 13 :
                        state->registers[reg_A] = data; 
                        break; 
                default : 
                        exit(EXIT_FAILURE);              
        }
        (void)data;
        (void)reg_A;
        (void)reg_B;
        (void)reg_C;
        return opcode;
}
/*
 Decodes a 32-bit instruction word into its components 
decoded_word decode(uint32_t word, decoded_word instruction) 
{
        instruction->opcode = (uint32_t)Bitpack_getu(word, 4, 28); 
        if (instruction->opcode == 13) { 
                instruction->reg_A = (uint32_t)Bitpack_getu(word, 3, 25); 
                instruction->reg_B = 8; 
                instruction->reg_C = 8; 
                instruction->data = (uint32_t)Bitpack_getu(word, 25, 0); 
                
        } else { 
        
                instruction->reg_A = (uint32_t)Bitpack_getu(word, 3, 6); 
                instruction->reg_B = (uint32_t)Bitpack_getu(word, 3, 3); 
                instruction->reg_C = (uint32_t)Bitpack_getu(word, 3, 0); 
                instruction->data = 0;
        }

        return instruction; 
}
*/
uint64_t Bitpack_getu(uint64_t word, unsigned width, unsigned lsb)
{
        assert(width <= 64);
        assert(width + lsb <= 64);
            
        if (width == 64) {
                return word;
        }
            
        uint64_t mask1 = (1 << (width)) - 1;
        uint64_t mask = (mask1 << lsb);
            
        uint64_t newWord = (word & mask) >> lsb;
        
        return newWord;
}

uint64_t Bitpack_newu(uint64_t word, unsigned width, unsigned lsb, 
                      uint64_t value)
{
        assert(width <= 64);
        assert(width + lsb <= 64);
        
        if (Bitpack_fitsu(value, width)) {
                uint64_t mask1 = (1 << (width)) - 1; 
                uint64_t mask2 = (mask1 << lsb);
                uint64_t mask = ~mask2;
        
                word &= mask;
                word |= (value << lsb);
                
                return word;
        }
        RAISE(Bitpack_Overflow); {
        }

        /* the function should never reach this point, but we need to return*/
        /* a word for the program to compile*/
        return word;
}

bool Bitpack_fitsu(uint64_t n, unsigned width)
{
        unsigned maxVal = ((1 << (width)) - 1);
        
        if(width == 0) {
                return false;
        }
        if (n <= maxVal) {
                return true;
        }
        else {
                return false;
        }
}

uint32_t create_segment(program_state state, uint32_t length)
{       
        uint32_t *mem_segment = malloc(sizeof(int) * length);
        assert(mem_segment != NULL); 
        uint32_t *mem_length = malloc(sizeof(int)); 
        assert(mem_length != NULL);
        *mem_length = length;   
        for (uint32_t i = 0; i < length; i++) {
                mem_segment[i] = 0;
        }
        /* Tries to reuse old segemtns first */
        if (Stack_empty(state->unused_ids) == 0) { 
                uint32_t *segment_id_ptr =
                                ((uint32_t *) Stack_pop(state->unused_ids));
                uint32_t segment_id = *segment_id_ptr;
                Seq_put(state->memory, segment_id, mem_segment);
                Seq_put(state->seg_lengths, segment_id, mem_length); 
                free(segment_id_ptr);
                return segment_id;  
        } else { 
                Seq_addhi(state->memory, mem_segment); 
                Seq_addhi(state->seg_lengths, mem_length); 
                return Seq_length(state->memory) - 1;  
        }       
}

/* Puts a word in segment at a location when given the segment ID and offset */
void put_word(program_state state, uint32_t segment_id, uint32_t offset,
              uint32_t word) 
{ 
        uint32_t *segment = (uint32_t *) Seq_get(state->memory, segment_id);
        segment[offset] = word; 
        return; 
} 

/* Deletes a segment and adds its ID to the stack for future reuse */
void delete_segment(program_state state, uint32_t segment_id)
{
        uint32_t *segment = (uint32_t *) Seq_get(state->memory, segment_id);
        uint32_t *length = (uint32_t*) Seq_get(state->seg_lengths, segment_id);
        uint32_t *id = malloc(sizeof(int));
        *id = segment_id;
        free(segment);  
        free(length); 
        segment = NULL;
        Seq_put(state->memory, segment_id, segment);
        length = NULL;
        Seq_put(state->seg_lengths, segment_id, length);
        Stack_push(state->unused_ids, id);
        return; 
}


