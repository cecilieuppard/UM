/****
 * segments.c 
 * Authors: Sonal Chatter & Cecilie Uppard
 * Last Modified: 11/24/15
 * 
 * Summary: Handles all creation and destruction of memory during a running 
 * program, and reuses previous used, but now unmapped, segments before 
 * creating new segments. 
 ****/

/* INCLUDED LIBRARIES */
#include <stdlib.h> 
#include <stdint.h>

/* INCLUDED INTERFACES */
#include "segments.h"
#include "stack.h" 
#include "seq.h"
#include "assert.h"

/* FUNCTION IMPLEMENTATIONS */

/* Creates a segment and returns its identifier */
uint32_t create_segment(program_state state, uint32_t length)
{       
        uint32_t *mem_segment = malloc(sizeof(int) * length);
        assert(mem_segment != NULL); 
        uint32_t *mem_length = malloc(sizeof(int)); 
        assert(mem_length != NULL);
        *mem_length = length;   
        for (uint32_t i = 0; i < length; i++) {
                mem_segment[i] = 0;
        }
        /* Tries to reuse old segemtns first */
        if (Stack_empty(state->unused_ids) == 0) { 
                uint32_t *segment_id_ptr =
                                ((uint32_t *) Stack_pop(state->unused_ids));
                uint32_t segment_id = *segment_id_ptr;
                Seq_put(state->memory, segment_id, mem_segment);
                Seq_put(state->seg_lengths, segment_id, mem_length); 
                free(segment_id_ptr);
                return segment_id;  
        } else { 
                Seq_addhi(state->memory, mem_segment); 
                Seq_addhi(state->seg_lengths, mem_length); 
                return Seq_length(state->memory) - 1;  
        }       
}

/* Puts a word in segment at a location when given the segment ID and offset */
void put_word(program_state state, uint32_t segment_id, uint32_t offset,
              uint32_t word) 
{ 
        uint32_t *segment = (uint32_t *) Seq_get(state->memory, segment_id);
        segment[offset] = word; 
        return; 
} 

/* Deletes a segment and adds its ID to the stack for future reuse */
void delete_segment(program_state state, uint32_t segment_id)
{
        uint32_t *segment = (uint32_t *) Seq_get(state->memory, segment_id);
        uint32_t *length = (uint32_t*) Seq_get(state->seg_lengths, segment_id);
        uint32_t *id = malloc(sizeof(int));
        *id = segment_id;
        free(segment);  
        free(length); 
        segment = NULL;
        Seq_put(state->memory, segment_id, segment);
        length = NULL;
        Seq_put(state->seg_lengths, segment_id, length);
        Stack_push(state->unused_ids, id);
        return; 
}
