/****
 * execute_program.c 
 * Authors: Sonal Chatter & Cecilie Uppard
 * Last Modified: 11/24/15
 * 
 * Summary: Executes a program given a valid file and filesize. It accepts 
 * 32 bit words for instructions and decodes them.  This machine only runs 14
 * instructions. 
 ****/

/* INCLUDED INTERFACES */
#include "execute_program.h"
#include "segments.h" 
#include "bitpack.h" 
#include "assert.h"

#define POWER_2_32 4294967296
#define INITIAL_SIZE 10

/* Holds the unpacked values from a single instruction word  */
typedef struct decoded_word {
        uint32_t opcode, reg_A, reg_B, reg_C, data;
}*decoded_word;

/* Various Helper functions that each deal with a specific aspect */
void run_instruction(program_state state, decoded_word instruction);
void destruct(program_state state);
program_state initialize(FILE* fp, int prog_length);
decoded_word decode(uint32_t word, decoded_word instruction);

/* Controls and executes each instruction */
void execute_prog(FILE *fp, int prog_length)
{ 
        program_state state = initialize(fp, prog_length);
        uint32_t *segment_zero = Seq_get(state->memory, 0); 
        decoded_word word = malloc(sizeof(struct decoded_word));
        assert(word != NULL);
        while(1) {
                word = decode(segment_zero[state->instruction_counter], word); 
                run_instruction(state, word);
                /* handles special cases, load program(12) and halt(7) */
                if(word->opcode == 12) { 
                        segment_zero = Seq_get(state->memory, 0); 
                }
                if (word->opcode == 7){
                        break;
                }
                state->instruction_counter++; 
        }
        free(word); 
        return; 
}
/* Initializes the program_state  */
program_state initialize(FILE* fp, int prog_length) {
        program_state state = malloc(sizeof(struct program_state)); 
        assert(state != NULL);
        uint32_t *seg_0_length = malloc(sizeof(uint32_t)); 
        assert(seg_0_length != NULL);
        state->program_length = (uint32_t)prog_length;
        uint32_t *segment0 = malloc(sizeof(uint32_t) * state->program_length);
        assert(segment0 != NULL);
        state->memory = Seq_new(INITIAL_SIZE);
        state->seg_lengths = Seq_new(INITIAL_SIZE); 
        state->unused_ids = Stack_new();

        /*mallocing length of program to store in program length sequence */
        for (uint32_t i = 0; i < state->program_length; i++) {
                uint32_t word = 0; 
                int c = fgetc(fp); 
                word = Bitpack_newu(word, 8, 24, c);
                word = Bitpack_newu(word, 8, 16, fgetc(fp));
                word = Bitpack_newu(word, 8, 8, fgetc(fp));
                word = Bitpack_newu(word, 8, 0, fgetc(fp));
                segment0[i] = word;     
        }
        
        *seg_0_length = prog_length; 
        state->program_ptr = &segment0[0]; 
        Seq_addlo(state->memory, segment0);
        Seq_addlo(state->seg_lengths, seg_0_length); 
        state->registers = (uint32_t*)malloc(sizeof(uint32_t)*8);
        /* initializing all registers to zero */        
        for(uint32_t i = 0; i < 8; i++) { 
                state->registers[i] = 0; 
        }
        state->instruction_counter = 0; 
        return state;
}
 
/* Destructs the program state and allows for a successful exit  */
void destruct(program_state state)
{
        int seq_length = Seq_length(state->memory); 
        for(int i = 0; i < seq_length; i++) {
                uint32_t *temp = Seq_get(state->memory, i);
                uint32_t *temp2 = Seq_get(state->seg_lengths, i); 
                if(temp != NULL) {
                        free(temp);
                }
                if (temp != NULL) { 
                        free(temp2);
                }
        }
        Seq_free(&state->memory);
        Seq_free(&state->seg_lengths);
        while (Stack_empty(state->unused_ids) == 0) { 
                uint32_t *id = (uint32_t *)Stack_pop(state->unused_ids);
                free(id); 
        }
        Stack_free(&state->unused_ids); 
        free(state->registers);
        free(state); 
        return; 
} 

/* Runs a given instruction, exit failure if opcode is not valid */ 
void run_instruction(program_state state, decoded_word instruction) { 
        uint32_t opcode = instruction->opcode; 
        uint32_t reg_A = instruction->reg_A;
        uint32_t reg_B = instruction->reg_B;
        uint32_t reg_C = instruction->reg_C;
        uint32_t data = instruction->data;      
        uint32_t *segment = NULL;
        uint32_t identifier;
        uint32_t length;
        char input = NULL;  
        switch(opcode) { 
                /* Conditional Move */
                case 0 :
                        if (state->registers[reg_C] != 0) {
                                state->registers[reg_A] = 
                                                state->registers[reg_B];
                        }
                        return; 
                /* Segmented Load */
                case 1 :
                        segment = Seq_get(state->memory, 
                                        state->registers[reg_B]);
                        state->registers[reg_A] = 
                                        segment[state->registers[reg_C]];
                        return; 
                /* Segmented Store */
                case 2 : 
                        segment = Seq_get(state->memory, 
                                        state->registers[reg_A]);
                        segment[state->registers[reg_B]] = 
                                        state->registers[reg_C];
                        return; 
                /* Addition */
                case 3 : 
                        state->registers[reg_A] = (state->registers[reg_B] +
                                        state->registers[reg_C]) % POWER_2_32;  
                        return;
                /* Multiplication */
                case 4 : 
                        state->registers[reg_A] = (state->registers[reg_B] *
                                        state->registers[reg_C]) % POWER_2_32;  
                        return; 
                /* Division */
                case 5 : 
                        state->registers[reg_A] = (state->registers[reg_B] /
                                        state->registers[reg_C]);  
                        return; 
                /* Bitwise NAND */
                case 6 : 
                        state->registers[reg_A] = ~(state->registers[reg_B] &
                                        state->registers[reg_C]);  
                        return;
                /* Halt */
                case 7 :
                        destruct(state);
                        return; 
                /* Map Segment */ 
                case 8 :
                        
                        identifier = create_segment(state, 
                                        state->registers[reg_C]);
                        state->registers[reg_B] = identifier;   
                        return; 
                /* Unmap Segment */
                case 9 : 
                        delete_segment(state, state->registers[reg_C]);
                        return; 
                /* Output */
                case 10 :
                        if ((state->registers[reg_C] <= 255)) {
                                fprintf(stdout,"%c",state->registers[reg_C]);
                        }
                        return; 
                /* Input */  
                case 11 :
                        input = fgetc(stdin);
                        if (input == EOF) {
                                state->registers[reg_C] = ~0;
                        } else {
                                state->registers[reg_C] = input;
                        }
                        return; 
                /* Load Program */
                case 12 : 
                        if(state->registers[reg_B] == 0) { 
                                state->instruction_counter = 
                                                state->registers[reg_C]-1;
                                return;
                        }
                        segment = Seq_get(state->memory, 
                                        state->registers[reg_B]);
                        length = *((uint32_t *)(Seq_get(state->seg_lengths, 
                                        state->registers[reg_B])));
                        delete_segment(state, 0);
                        identifier = create_segment(state, length);
                        for (uint32_t i = 0; i < length; i++) {
                                put_word(state, identifier, i, segment[i]);
                        }
                        state->instruction_counter = state->registers[reg_C]-1;
                        return; 
                /* Load Value */ 
                case 13 :
                        state->registers[reg_A] = data; 
                        return; 
                default : 
                        exit(EXIT_FAILURE);              
        }
        return;
}

/* Decodes a 32-bit instruction word into its components */
decoded_word decode(uint32_t word, decoded_word instruction) 
{
        instruction->opcode = (uint32_t)Bitpack_getu(word, 4, 28); 
        if (instruction->opcode == 13) { 
                instruction->reg_A = (uint32_t)Bitpack_getu(word, 3, 25); 
                instruction->reg_B = 8; 
                instruction->reg_C = 8; 
                instruction->data = (uint32_t)Bitpack_getu(word, 25, 0); 
                
        } else { 
        
                instruction->reg_A = (uint32_t)Bitpack_getu(word, 3, 6); 
                instruction->reg_B = (uint32_t)Bitpack_getu(word, 3, 3); 
                instruction->reg_C = (uint32_t)Bitpack_getu(word, 3, 0); 
                instruction->data = 0;
        }

        return instruction; 
}
