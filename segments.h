/****
 * segments.h 
 * Authors: Sonal Chatter & Cecilie Uppard
 * Last Modified: 11/24/15
 ****/

#ifndef SEGMENTS_INCLUDED
#define SEGMENTS_INLCUDED

/* INCLUDED LIBRARIES */
#include <stdint.h>

/*INCLUDED INTERFACES */
#include "execute_program.h" 

/* FUNCTION DECLARATIONS */ 
uint32_t create_segment(program_state state, uint32_t size); 
void put_word(program_state state, uint32_t segment_id, uint32_t offset,
              uint32_t word); 
void delete_segment(program_state state, uint32_t segment_id); 
 
#endif
