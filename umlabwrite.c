#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include "assert.h"
#include "fmt.h"
#include "seq.h"
#include "bitpack.h"

extern void Um_write_sequence(FILE *output, Seq_T asm)
{
	int length = Seq_length(asm);
	char byte;
	for (int i = 0; i < length; i++) {
		uint32_t word = (uintptr_t)Seq_get(asm, i);
		for(int j = 8; j <= 32; j+=8) { 
			byte = Bitpack_getu(word, 8, 32-j);
			fputc(byte,output); 
		}		
	} 	
}

extern void emit_halt_test(Seq_T asm);
extern void emit_goto_test(Seq_T asm);
extern void emit_cmov_zero_test(Seq_T asm);
extern void emit_mapsegment_test(Seq_T asm);
extern void emit_loadprogram_test(Seq_T asm);
extern void emit_addition_test(Seq_T asm);
extern void emit_multiplication_test(Seq_T asm);
extern void emit_division_test(Seq_T asm);
extern void emit_nand_test(Seq_T asm);
extern void emit_unmapsegment_test(Seq_T asm);
extern void emit_output_test(Seq_T asm);
extern void emit_loadstore_test(Seq_T asm);
extern void emit_input_test(Seq_T asm);

static struct test_info {
        const char *name;
        const char *test_input;            /* NULL means no input needed */
        const char *expected_output;
        /* writes instructions into sequence */
        void (*emit_test) (Seq_T stream);
} tests[] = {
        {"halt", NULL, "", emit_halt_test},
        {"goto", NULL, "GOTO passed.\n", emit_goto_test},
	{"cmov", NULL, "95\n55\n", emit_cmov_zero_test},
	{"mapseg", NULL, "", emit_mapsegment_test},
	{"loadprog", NULL, "YAY", emit_loadprogram_test},
	{"add", NULL, "8\n", emit_addition_test},
	{"mul", NULL, "2", emit_multiplication_test},
	{"div", NULL, "2", emit_division_test},
	{"nand", NULL, "", emit_nand_test},
	{"unmapseg", NULL, "", emit_unmapsegment_test},
	{"out", NULL, "Hello", emit_output_test},
	{"loadstore", NULL, "2", emit_loadstore_test},
	{"in", "Hi", "Hi", emit_input_test},
};

#define NTESTS (sizeof(tests)/sizeof(tests[0]))

/*
 * open file 'path' for writing, then free the pathname;
 * if anything fails, checked runtime error
 */
static FILE *open_and_free_pathname(char *path);

/*
 * if contents is NULL or empty, remove the given 'path', 
 * otherwise write 'contents' into 'path'.  Either way, free 'path'.
 */
static void write_or_remove_file(char *path, const char *contents);

static void write_test_files(struct test_info *test)
{
        FILE *binary = open_and_free_pathname(Fmt_string("%s.um", test->name));
        Seq_T asm = Seq_new(0);
        test->emit_test(asm);
        Um_write_sequence(binary, asm);
        Seq_free(&asm);
        fclose(binary);

        write_or_remove_file(Fmt_string("%s.0", test->name), test->test_input);
        write_or_remove_file(Fmt_string("%s.1", test->name),
                             test->expected_output);
}

int main(int argc, char *argv[])
{
        bool failed = false;
        if (argc == 1)
                for (unsigned i = 0; i < NTESTS; i++) {
                        printf("***** Writing test '%s'.\n", tests[i].name);
                        write_test_files(&tests[i]);
        } else
                for (int j = 1; j < argc; j++) {
                        bool tested = false;
                        for (unsigned i = 0; i < NTESTS; i++)
                                if (!strcmp(tests[i].name, argv[j])) {
                                        tested = true;
                                        write_test_files(&tests[i]);
                                }
                        if (!tested) {
                                failed = true;
                                fprintf(stderr,
                                        "***** No test named %s *****\n",
                                        argv[j]);
                        }
                }
        return failed;   /* failed nonzero == exit nonzero == failure */
}

static void write_or_remove_file(char *path, const char *contents)
{
        if (contents == NULL || *contents == '\0') {
                remove(path);
        } else {
                FILE *input = fopen(path, "wb");
                assert(input);
                fputs(contents, input);
                fclose(input);
        }
        free(path);
}

static FILE *open_and_free_pathname(char *path)
{
        FILE *fp = fopen(path, "wb");
        assert(fp != NULL);

        free(path);
        return fp;
}
