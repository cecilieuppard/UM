README 

Authors: Sonal Chatter & Cecilie Uppard 

Date: 11/24/15

Shoutouts: Noah, Carter, Charlie, Max, Kabir, Jacob, Peter, Justin, Margaret,
	   Dan, Dariusz, & StackOverflow

All modules, interfaces, and the overall solution to the problem have been 
implemented correctly to the best of our knowledge.
______________________________________________________________________________

Notes on Departures from Original Design Plan: 

We have collapsed the separate power_up_down, decoder, and instruction modules
into execute_program. While there are separate functions for everything, we 
decided that having 4 separate files, of which 2 only held  one function, did
not seem like a good way to modularize. Instead of many files, we made sure 
each function we wrote in execute_program did exactly one thing. 
______________________________________________________________________________

Notes on the Architecture of our Solution: 

The architecture of our program is relatively straightforward and follows our 
design in spirit. main.c handles opening and closing a file and getting 
filesize. Execute_program handles initializing the program state, executing 
each instruction (decoding it first), and freeing all memory. Segments handles
the creation and destruction of memory segments. 
_____________________________________________________________________________

Notes on UM Speed: 

Our UM runs about 37 million instructions/second. We came to this conclusion 
based on the following math. 

               # of Instructions     Time(sec)  	Instructions/Time
midmark.um   	   ~80million          53.89s		   ~37million
sandmark.umz 	   ~2billion           2.15s		   ~37million

If 37 million instructions are computed in one second then 50 million 
instructions will take 50/37 seconds. 

50/37 = ~1.35. Thus our program completes 50 million instruction in 1.35s.  
_____________________________________________________________________________

Notes on UM Unit Tests: 

1) "halt.um"
Expected output: none
This test program was given to us in the lab assignment and tests if the halt 
instruction ends the program correctly.

2) "cmov.um"
Expected output: "95\n55\n"
This test program will load the values 0, 5 and 9 into register 1, 2 and 3
respectively. The program then performs a conditional move on register 1, which
should result in the registers being unchanged. The program then prints
register 2 and 3, which should be 5 and 9. The program then loads the value 2
into register 1 and performs the same conditional move. The registers 1 & 2 
should now both hold the value 5, and therefore print 55.

3) "mapseg.um"
Expected output: none
This test program will simply map a segment.

4) "loadprog.um"
Expected output: "YAY"
This test program will give instructions to load the program in segment 0 and 
set the program counter to an instruction that starts printing the word "YAY".
If instruction was not successful the program will either fail or if the
program counter did not jump to the intended instruction the program will
print a sad face. 

5) "add.um"
Expected output: "8\n"
This test program adds two ascii values, 30 and 26, that should add up to 56,
the program should therefore print as the number 8 followed by a new line 
character. It also adds two 0s, without printing them.

6) "mul.um"
Expected output: "2"
This test program multiples two ascii values, 2 and 25, to add up to 50. when
printed the output should therefore be the decimal value 2. 

7) "div.um"
Expected output: "2"
This test program divides 100 by 2, which should result in 50. outputed the
ascii value should therefore be 2.

8) "nand.um"
Expected output: null
This test program nands together two equal values, both 127, which should
store the number 0 in a register. When that register is printed the output
should therefore be null. This is because when the same number is nanded 
correctly it should result in all 0s.

9) "unmapseg.um"
Expected output: none
This test program maps a segment, which should end up at identifier 1(in our
case at least). The program then unmaps segment 0. if this operation is 
successful the program should just halt, rather then fail(which it will do
if it tries to unmap already unmapped segments or non existent segments)

10) "out.um"
Expected output: "Hello"
This program loads the letters "H", "e", "l", "o" into four different
registers and then prints the registers once, except for the one containing
"l", which it prints twice. 

11) "loadstore.um"
Expected output: "2"
This test program loads the values 3 into a register and maps a segment of 
that size. It then loads the values 1 and 2 into two other registers. It also
loads the value 50 into r4. Using the 1 as segment identifier and the number 2 
as segment offset it performs a segmented store of the value 50. It the
performs a segmented load using the same segment identifier and segment
offset, which should result in the program getting the value 50. It then prints
what it gets, which should be 2(as ascii 50 is 2 in dec)

12) "in.um"
Expected output: "Hi"
Using the input file in.0 the program should store "H" in a register and "i"
in another register. The program then outputs the two registers, and should
therefore print "Hi" 


_______________________________________________________
Hours Spent: 

Analyzing :                    3 hours 
Preparing the Design :         7 hours
Coding/Implementing/Testing : 15 hours	
Total Time :                  25 hours
______________________________________________________________________________
# UM
