/****
 * execute_program.h 
 * Authors: Sonal Chatter & Cecilie Uppard
 * Last Modified: 11/24/15
 ****/

#ifndef EXECUTE_PROGRAM_INCLUDED
#define EXECUTE_PROGRAM_INCLUDED

/* INCLUDED LIBRARIES */
#include <stdio.h> 
#include <stdlib.h> 
#include <stdint.h>

/* INCLUDED LIBRARIES */
#include "stack.h" 
#include "seq.h"

/* Holds the current state of a given program */
typedef struct program_state {
        uint32_t *program_ptr;
        Stack_T unused_ids; 
        Seq_T memory; 
        Seq_T seg_lengths;
        uint32_t *registers;
        uint32_t program_length;
        uint32_t instruction_counter; 
}*program_state;

/* FUNCTION DECLARATIONS */
void execute_prog(FILE *fp, int prog_length); 
 
#endif
