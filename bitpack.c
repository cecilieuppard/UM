/*
    by: Greta Jochem and Cecilie Uppard
    purpose: implementation of program that packs signed and unsigned integers
             into one word.
    date: 10/21/2015
*/

#include <bitpack.h>
#include <math.h>
#include <stdio.h>
#include <assert.h>
#include <except.h>
Except_T Bitpack_Overflow = { "Overflow packing bits" };

/* fitsu()
        purpose: check if a given unsigned 64 bit integer can be represented in 
                 a given number of bits
        parameters: n - number to check, width - number of bits to check if
                    n can be represented in
*/
bool Bitpack_fitsu(uint64_t n, unsigned width)
{
        unsigned maxVal = ((1 << (width)) - 1);
        
        if(width == 0) {
                return false;
        }
        if (n <= maxVal) {
                return true;
        }
        else {
                return false;
        }
}

/* fitsus()
        purpose: check if a given signed 64 bit integer can be represented in 
                 a given number of bits
        parameters: n - number to check, width - number of bits to check if
                    n can be represented in
*/
bool Bitpack_fitss(int64_t n, unsigned width)
{
        signed minVal = -((1 << (width)) / 2);
        signed maxVal = ((1 << (width)) / 2) - 1;
        
        if(width == 0) {
                return false;
        }
        if ((n >= minVal) && (n <= maxVal)) {
                return true;
        }
        else {
                return false;
        }
}

/* getu()
        purpose: get a bitfield of a given width from a uint64_t 
        parameters: word - uint64_t to get field from,
                    width - width of field
                    lsb - least significant bit of field
        return value: a uint64_t containing the value of the field
*/
uint64_t Bitpack_getu(uint64_t word, unsigned width, unsigned lsb)
{
        assert(width <= 64);
        assert(width + lsb <= 64);
            
        if (width == 64) {
                return word;
        }
            
        uint64_t mask1 = (1 << (width)) - 1;
        uint64_t mask = (mask1 << lsb);
            
        uint64_t newWord = (word & mask) >> lsb;
        
        return newWord;
}

/* gets()
        purpose: get a bitfield of a given width from a int64_t 
        parameters: word - uint64_t to get field from,
                    width - width of field
                    lsb - least significant bit of field
        return value: a int64_t containing the value of the field
*/
int64_t Bitpack_gets(uint64_t word, unsigned width, unsigned lsb) 
{ 
        uint64_t unsignedField = Bitpack_getu(word, width, lsb);
        
        /*shift returned bits all the way to the left*/
        int64_t shiftUnsignedField = unsignedField << (64-width); 
        
        /*shit bits all the way to the right, will be padded with */
        /*most significant bits*/
        int64_t newWord = shiftUnsignedField >> (64-width); 

        return newWord; 
}

/* newu()
        purpose: update a word with a given bitfield 
        parameters: word - uint64_t to put field in
                    width - width of field
                    lsb - least significant bit of field
                    value - a uint64_t representing the value to be put in the
                            word
        return value: a uint64_t containing the updated word
*/
uint64_t Bitpack_newu(uint64_t word, unsigned width, unsigned lsb, 
                      uint64_t value)
{
        assert(width <= 64);
        assert(width + lsb <= 64);
        
        if (Bitpack_fitsu(value, width)) {
                uint64_t mask1 = (1 << (width)) - 1; 
                uint64_t mask2 = (mask1 << lsb);
                uint64_t mask = ~mask2;
        
                word &= mask;
                word |= (value << lsb);
                
                return word;
        }
        RAISE(Bitpack_Overflow); {
        }

        /* the function should never reach this point, but we need to return*/
        /* a word for the program to compile*/
        return word;
}

/* news()
        purpose: update a word with a given int64_t bitfield 
        parameters: word - uint64_t to put field in
                    width - width of field
                    lsb - least significant bit of field
                    value - an int64_t representing the value to be put in the
                            word
        return value: a uint64_t containing the updated word
*/
uint64_t Bitpack_news(uint64_t word, unsigned width, unsigned lsb, 
                      int64_t value)
{
        assert(width <= 64);
        assert(width + lsb <= 64);
        
        if (Bitpack_fitss(value, width)) {
                uint64_t mask1 = (1 << (width)) - 1; 
                value &= mask1;
                uint64_t mask2 = (mask1 << lsb);
                uint64_t mask = ~mask2;
        
                word &= mask;
                word |= (value << lsb);
                
                return word;
        }
        RAISE(Bitpack_Overflow); {
        }
        
        /* the function should never reach this point, but we need to return*/
        /* a word for the program to compile*/
        return word;
}

