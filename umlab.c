#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>

#include "bitpack.h"
#include "seq.h"
#include "assert.h"
/* This is the code from the lab handout */


#define CM 0
#define SL 1
#define SS 2
#define ADD 3
#define MULT 4
#define DIV 5
#define NAND 6
#define HALT 7
#define MS 8
#define US 9
#define OUT 10
#define IN 11
#define LP 12
#define LV 13

typedef uint32_t Um_instruction;
typedef uint32_t Um_opcode;

Um_instruction three_register(Um_opcode op, int ra, int rb, int rc);
Um_instruction loadval(unsigned ra, unsigned val);

static inline Um_instruction halt(void)
{
	return three_register(HALT, 0, 0, 0);
}

static inline Um_instruction output(int rc)
{
	return three_register(OUT, 0, 0, rc); 		
}

static inline Um_instruction loadprogram(int rb, int rc)
{
	return three_register(LP, 0, rb, rc); 		
}

static inline Um_instruction conditionalmove(int ra, int rb, int rc)
{
	return three_register(CM, ra, rb, rc); 		
}

static inline Um_instruction segstore(int ra, int rb, int rc)
{
	return three_register(SS, ra, rb, rc);
}

static inline Um_instruction segload(int ra, int rb, int rc)
{
	return three_register(SL, ra, rb, rc);
}

static inline Um_instruction mapsegment(int rc)
{
	return three_register(MS, 0, 0, rc); 		
}

static inline Um_instruction addition(int ra, int rb, int rc)
{
	return three_register(ADD, ra, rb, rc); 		
}

static inline Um_instruction multiplication(int ra, int rb, int rc)
{
	return three_register(MULT, ra, rb, rc); 		
}

static inline Um_instruction division(int ra, int rb, int rc)
{
	return three_register(DIV, ra, rb, rc); 		
}

static inline Um_instruction nand(int ra, int rb, int rc)
{
	return three_register(NAND, ra, rb, rc); 		
}

static inline Um_instruction unmapsegment(int rc)
{
	return three_register(US, 0, 0, rc); 		
}

static inline Um_instruction input(int rc)
{
	return three_register(IN, 0, 0, rc); 		
}

Um_instruction three_register(Um_opcode op, int ra, int rb, int rc)
{
	Um_instruction word = 0;
	word = Bitpack_newu(word, 3, 0, rc);
	word = Bitpack_newu(word, 3, 3, rb);
	word = Bitpack_newu(word, 3, 6, ra);
	word = Bitpack_newu(word, 4, 28, op);

	return word;
}

Um_instruction loadval(unsigned ra, unsigned val) 
{
	Um_instruction word = 0;
	word = Bitpack_newu(word, 4, 28, 13);
	word = Bitpack_newu(word, 3, 25, ra);
	word = Bitpack_newu(word, 25, 0, val);

	return word;
}

enum regs { r0 = 0, r1, r2, r3, r4, r5, r6, r7 };

extern void Um_write_sequence(FILE *output, Seq_T stream);

static inline void emit(Seq_T stream, Um_instruction inst)
{
	assert(sizeof(inst) <= sizeof(uintptr_t));
	Seq_addhi(stream, (void *)(uintptr_t) inst);
}

static inline Um_instruction get_inst(Seq_T stream, int i)
{
	assert(sizeof(Um_instruction) <= sizeof(uintptr_t));
	return (Um_instruction) (uintptr_t) (Seq_get(stream, i));
}

static inline void put_inst(Seq_T stream, int i, Um_instruction inst)
{
	assert(sizeof(inst) <= sizeof(uintptr_t));
	Seq_put(stream, i, (void *)(uintptr_t) inst);
}

void emit_halt_test(Seq_T stream)
{
	emit(stream, halt());
	emit(stream, loadval(r1, 'B'));
	emit(stream, output(r1));
	emit(stream, loadval(r1, 'a'));
	emit(stream, output(r1));
	emit(stream, loadval(r1, 'd'));
	emit(stream, output(r1));
	emit(stream, loadval(r1, '!'));
	emit(stream, output(r1));
	emit(stream, loadval(r1, '\n'));
	emit(stream, output(r1));
}

static void add_label(Seq_T stream, int location_to_patch, int label_value)
{
	Um_instruction inst = get_inst(stream, location_to_patch);
	unsigned k = Bitpack_getu(inst, 25, 0);
	inst = Bitpack_newu(inst, 25, 0, label_value + k);
	put_inst(stream, location_to_patch, inst);
}

static void emit_out_string(Seq_T stream, const char *s, int aux_reg)
{
	(void)stream;
	int i = 0;
	while(s[i] != '\0') {
		emit(stream, loadval(aux_reg, s[i]));
		emit(stream, output(aux_reg));
		i++;
	}
}

void emit_goto_test(Seq_T stream)
{
	int patch_L = Seq_length(stream);
	emit(stream, loadval(r7, 0));	     /* will be patched to 'r7 := L' */
	emit(stream, loadprogram(r0, r7));   /* should goto label L          */
	emit_out_string(stream, "GOTO failed.\n", r1);
	emit(stream, halt());
	/* define 'L' to be here */
	add_label(stream, patch_L, Seq_length(stream));	
	emit_out_string(stream, "GOTO passed.\n", r1);
	emit(stream, halt());
} 

void emit_cmov_zero_test(Seq_T stream) { 
	
	emit(stream, loadval(r3, 0));
	emit(stream, loadval(r2, 53));
	emit(stream, loadval(r1, 57));
	emit(stream, conditionalmove(r1, r2, r3));
	emit(stream, output(r1));
	emit(stream, output(r2));
	emit(stream, loadval(r4, '\n'));
	emit(stream, output(r4));
	emit(stream, loadval(r3, 50));
	emit(stream, conditionalmove(r1, r2, r3));
	emit(stream, output(r1));
	emit(stream, output(r2));
	emit(stream, loadval(r4, '\n'));
	emit(stream, output(r4));
	emit(stream, halt());
}

void emit_mapsegment_test(Seq_T stream) 
{
	emit(stream, loadval(r2, 10));
	emit(stream, mapsegment(r2));
}

void emit_loadprogram_test(Seq_T stream)
{
	emit(stream, loadval(r1, 0));
	emit(stream, loadval(r2, 12));
	emit(stream, output(r4));
	emit(stream, loadprogram(r1, r2));
	emit(stream, loadval(r4, ':'));
	emit(stream, output(r4));
	emit(stream, loadval(r4, '-'));
	emit(stream, output(r4));
	emit(stream, loadval(r4, '('));
	emit(stream, output(r4));
	emit(stream, loadval(r4, '\n'));
	emit(stream, output(r4));
	emit(stream, loadval(r4, 'Y'));
	emit(stream, output(r4));
	emit(stream, loadval(r4, 'A'));
	emit(stream, output(r4));
	emit(stream, loadval(r4, 'Y'));
	emit(stream, output(r4));
	emit(stream, loadval(r4, '\n'));
	emit(stream, halt());
}

void emit_addition_test(Seq_T stream) 
{
	emit(stream, loadval(r1, 30));
	emit(stream, loadval(r2, 26));
	emit(stream, addition(r3, r1, r2));
	emit(stream, output(r3));
	emit(stream, loadval(r4, '\n'));
	emit(stream, output(r4));
	emit(stream, loadval(r1, 0));
	emit(stream, loadval(r2, 0));
	emit(stream, addition(r3, r1, r2));
	emit(stream, halt());
}

void emit_multiplication_test(Seq_T stream)
{
	emit(stream, loadval(r1, 2));
	emit(stream, loadval(r2, 25));
	emit(stream, multiplication(r3, r2, r1));
	emit(stream, output(r3));
	emit(stream, halt());
}

void emit_division_test(Seq_T stream)
{
	emit(stream, loadval(r1, 100));
	emit(stream, loadval(r2, 2));
	emit(stream, division(r3, r1, r2));
	emit(stream, output(r3));
	emit(stream, halt());
}

void emit_nand_test(Seq_T stream)
{
	emit(stream, loadval(r1, 127));
	emit(stream, loadval(r2, 127));
	emit(stream, nand(r3, r2, r1));
	emit(stream, output(r3));
	emit(stream, halt());
}

void emit_unmapsegment_test(Seq_T stream)
{
	emit(stream, loadval(r1, 2));
	emit(stream, mapsegment(r1));
	emit(stream, loadval(r2, 1));
	emit(stream, unmapsegment(r2));
	emit(stream, halt());
}

void emit_output_test(Seq_T stream)
{
	emit(stream, loadval(r1, 'H'));
	emit(stream, loadval(r2, 'e'));
	emit(stream, loadval(r3, 'l'));
	emit(stream, loadval(r4, 'o'));
	emit(stream, output(r1));
	emit(stream, output(r2));
	emit(stream, output(r3));
	emit(stream, output(r3));
	emit(stream, output(r4));
	emit(stream, halt());
}

void emit_loadstore_test(Seq_T stream)
{
	emit(stream, loadval(r1, 3));
	emit(stream, mapsegment(r1));
	emit(stream, loadval(r3, 1));
	emit(stream, loadval(r2, 2));
	emit(stream, loadval(r4, 50));
	emit(stream, segstore(r3, r2, r4));
	emit(stream, segload(r5, r3, r2));
	emit(stream, output(r5));
	emit(stream, halt()); 
}

void emit_input_test(Seq_T stream)
{
	emit(stream, input(r1));
	emit(stream, input(r2));
	emit(stream, output(r1));
	emit(stream, output(r2));
	emit(stream, halt());
}

